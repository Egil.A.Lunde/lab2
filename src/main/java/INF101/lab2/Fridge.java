package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    List<FridgeItem> fridgeItems = new ArrayList<FridgeItem>();
    int max_items = 20; 
    public int totalSize(){
        return max_items;
    }
    @Override
    public int nItemsInFridge() {
        int nItemfrigdeint = 0;
        int nItemfridgeint = nItemfrigdeint + fridgeItems.size();
        return nItemfridgeint;
    }
    @Override
    public boolean placeIn(FridgeItem item) {
        if(fridgeItems.size() < max_items){
            fridgeItems.add(item);
            return true;
        }
        return false;
    }
    @Override
    public void takeOut(FridgeItem item) {
        if(fridgeItems.contains(item)){
            fridgeItems.remove(item);
        }
        else if(fridgeItems.isEmpty()){
            throw new NoSuchElementException();
        }
        
    }
    @Override
    public void emptyFridge() {
        fridgeItems.clear();
        
    }
    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for(FridgeItem item : this.fridgeItems){
            if(item.hasExpired()==true){
                expiredFood.add(item);
                
            }
            
        }
        this.fridgeItems.removeAll(expiredFood);
        return expiredFood;
    }
}
